<?php

use Doctum\Doctum;
use Doctum\Version\GitVersionCollection;
use Rtdm\DrupalGitLabRemoteRepository;
use Symfony\Component\Finder\Finder;

$dir = __DIR__ . '/remote-src';
$iterator = Finder::create()
  ->files()
  ->name('*.php')
  ->in($dir)
  ->notPath('#^core/tests/.*#')
  ->notPath('#^core/modules/[a-zA-Z0-9_]*/tests/.*#');

/** @var \Doctum\Version\GitVersionCollection $versions */
$versions = new GitVersionCollection($dir);
$versions->setFilter(static function (string $version): bool {
  foreach (['rc', 'beta', 'alpha'] as $str) {
    if (strpos(strtolower($version), $str) !== false) {
      return false;
    }
  }

  return true;
});
$versions
  ->add('9.4.x', '9.4 (In Development)')
  ->add('9.3.0', '9.3')
  ->add('9.2.9', '9.2')
  ->add('8.9.20', 'Drupal 8 (EOL)')
  ->add('10.0.x', 'Drupal 10 (In Development)');

return new Doctum($iterator, [
  'versions'             => $versions,
  'title'                => 'Drupal Core API Explorer',
  'language'             => 'en', // Could be 'fr'
  'build_dir'            => __DIR__ . '/build/drupal-core/%version%',
  'cache_dir'            => __DIR__ . '/cache/%version%',
  'remote_repository'    => new DrupalGitLabRemoteRepository('project/drupal', dirname($dir), 'https://git.drupalcode.org/'),
  'default_opened_level' => 1,
  'footer_link'          => [
    'href'        => 'https://rtdm.info',
    'target'      => '_blank',
    'link_text'   => 'RTDM',
    'after_text'  => ' - Read the Drupal Docs',
  ],
]);
