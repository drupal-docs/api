<?php

declare(strict_types=1);

namespace Rtdm;

use Doctum\RemoteRepository\GitLabRemoteRepository;

final class DrupalGitLabRemoteRepository extends GitLabRemoteRepository {

  public function getFileUrl($projectVersion, $relativePath, $line) {
    $url = $this->url . $this->name . '/-/blob/' . $this->buildProjectPath($projectVersion, $relativePath);

    if (null !== $line) {
      $url .= '#L' . (int) $line;
    }

    return $url;
  }

  protected function buildProjectPath(string $projectVersion, string $relativePath): string
  {
    [,$relativePath] = explode('/', ltrim($relativePath, '/'), 2);
    return sprintf('%s/%s', $projectVersion, $relativePath);
  }

}
